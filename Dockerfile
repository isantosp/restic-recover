FROM cern/cc7-base

ARG restic_version=0.9.6


RUN yum install yum-plugin-copr -y && \
    yum copr enable copart/restic -y && \
    yum install restic-${restic_version} -y && \
    yum install s3cmd -y && \
    yum clean all

ENTRYPOINT while true; do sleep 60; done

